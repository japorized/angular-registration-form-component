# Angular Registration Form Assignment

This is partly a project to learn Angular

---

## Setup

```
git clone https://gitlab.com/japorized/angular-registration-form-component
cd angular-registration-form-component
npm ci
```
---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `npm run ng generate component component-name` to generate a new component. You can also use `npm run ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


---

## Notes

* `aot` in `angular.json` is set to `false` as a workaround to the following error.
```
    ERROR in node_modules/angular-mydatepicker/lib/angular-mydatepicker.module.d.ts:1:22 - error NG6002: Appears in the NgModule.imports of AppModule, but could not be resolved to an NgModule class

    1 export declare class AngularMyDatePickerModule {
                           ~~~~~~~~~~~~~~~~~~~~~~~~~
```
* I found about about FormControl a little too late. The code can definitely be
a bit more modular should I realize this feature of Angular sooner.
* I apologize that I have only one unit test (aside from the templated one)
written, and now that component is not even used. I ran into the problem of
not being able to properly set values and have Karma detect them, which led
me to finding out about FormControl, which in turn led to me ditching the component
for lack of knowing how to pass `formControlName` for a component.
* Looks like most of these problems stemmed from me not knowing about the
items listed in item 10.

---

## Known Issues

* `*ngFor` in `src/app/users/users.component` is not working
