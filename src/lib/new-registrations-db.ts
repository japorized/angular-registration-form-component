export const newUsers: {
  honorific: number;
  firstName: string;
  lastName: string;
  email: string;
  confirmEmail: string;
  dateOfBirth: string;
  phoneNumber: string;
}[] = [
  {
    honorific: 1,
    firstName: "Johnathon",
    lastName: "Ting",
    email: "johnathon.ting.ks@protonmail.com",
    confirmEmail: "johnathon.ting.ks@protonmail.com",
    dateOfBirth: "15/12/1998",
    phoneNumber: "012-23001-012"
  },
  {
    honorific: 2,
    firstName: "Samantha",
    lastName: "Smith",
    email: "samantha.smith@gmail.com",
    confirmEmail: "samantha.smith@gmail.com",
    dateOfBirth: "11/04/2000",
    phoneNumber: "019-85930-103"
  },
  {
    honorific: 3,
    firstName: "Richard",
    lastName: "Feynman",
    email: "rfeynman@mit.edu",
    confirmEmail: "rfeynman@mit.edu",
    dateOfBirth: "11/05/1918",
    phoneNumber: "015-59381-304"
  }
];
