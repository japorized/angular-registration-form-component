import { Component } from "@angular/core";

@Component({
  selector: "users-list",
  templateUrl: "./users.component.html"
})
export class Users {
  users: {
    honorific: number;
    firstName: string;
    lastName: string;
    email: string;
    dateOfBirth: string;
    phoneNumber: string;
  }[] = [];

  ngOnInit() {
    const rawUsers = window.sessionStorage.getItem('users');
    let storedUsers: any;
    if (rawUsers !== null) {
      storedUsers = JSON.parse(window.sessionStorage.getItem('users'));
    } else {
      storedUsers = [];
    }
    for (const u of storedUsers) {
      this.users.push({
        honorific: Number(u.honorific),
        firstName: u.firstName,
        lastName: u.lastName,
        email: u.email,
        dateOfBirth: u.dateOfBirth,
        phoneNumber: u.phoneNumber
      });
    }

    this.users = this.users.reverse();
  }
}
