import { Component, Input } from "@angular/core";

@Component({
  selector: "input-tel",
  styleUrls: ['./form-style.styl'],
  templateUrl: "./input-tel.component.html"
})
export class InputTel {
  @Input() name: string;
  @Input() placeholder: string;
  @Input() required: boolean;

  telFormatting(event: any) {
    if (event.key !== 'Backspace') {
      const target = event.target;
      if (target.value.length === 3) {
        target.value = target.value.replace(/([0-9]{3})/, '\$1-');
      } else if (target.value.length > 3 && target.value.length < 10) {
        target.value = target.value.replace(/([0-9]{3}-[0-9]{5})/, '\$1-');
      }
    }
  }
}
