import { Component, Input } from "@angular/core";

@Component({
  selector: 'input-text',
  styleUrls: ['./form-style.styl', './input-text.component.styl'],
  templateUrl: './input-text.component.html'
})
export class InputText {
  @Input() name: string;
  @Input() placeholder: string;
  @Input() required: boolean;
  @Input() validityMsg: string;

  patternValidity(event: any) {
    const val: string = event.target.value;
    const tooltip = document.querySelector('#tooltip-' + this.name);
    if (/[a-zA-Z\s]+/.test(val) || val.length === 0) {
      tooltip.classList.remove('active');
    } else {
      tooltip.classList.add('active');
    }
  }
}
