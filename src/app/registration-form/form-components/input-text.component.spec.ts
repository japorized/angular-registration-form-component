import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { InputText } from './input-text.component';

describe('InputText', () => {
  let fixture: ComponentFixture<InputText>;
  let component: InputText;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        InputText
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(InputText);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should show pattern message if input contains non-alphabets', (done) => {
    component.name = 'full-name';
    component.required = true;
    component.placeholder = 'Full name';
    component.validityMsg = 'Please enter alphabets and whitespaces only';
    const input = fixture.debugElement.query(By.css('input[type=text]'));
    const tooltip = fixture.debugElement.query(By.css('div[role=tooltip]'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      input.nativeElement.value = "123";
      input.nativeElement.dispatchEvent(new Event('keyup'));
      fixture.detectChanges();
      expect(tooltip.nativeElement.classList.contains('active')).toBeTruthy();
      done();
    })
  });
});
