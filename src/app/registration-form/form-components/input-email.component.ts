import { Component, Input } from "@angular/core";
import { emails } from 'src/lib/emails';

@Component({
  selector: "input-email",
  styleUrls: ["./form-style.styl"],
  templateUrl: "./input-email.component.html"
})
export class InputEmail {
  @Input() name: string;
  @Input() placeholder: string;
  @Input() required: boolean;
  validityMsg: string;

  checkEmailExistence(event: any) {
    const val = event.target.value;
    const tooltip = document.querySelector('#tooltip-' + this.name);
    if (emails.includes(val)) {
      this.validityMsg = "Email address already exists";
      tooltip.classList.add('active');
    } else {
      tooltip.classList.remove('active');
    }
  }
}
