import { Component, Input } from "@angular/core";
import { IAngularMyDpOptions, IMyDateModel } from "angular-mydatepicker";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "date-picker",
  styleUrls: ["./form-style.styl", "./datetime.component.styl"],
  templateUrl: "./datetime.component.html"
})
export class DatePicker {
  @Input() name: string;
  @Input() required: boolean;
  shouldInitDate: boolean = true;
  dateModel: IMyDateModel = null;
  datePickerOptions: IAngularMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    dateRange: false,
    markCurrentDay: true,
    stylesData: {
      selector: "dp",
      styles: `
      .dp .myDpWeekDayTitle {
        background-color: deeppink;
      }
      `
    }
  };
  faCalendarAlt = faCalendarAlt;

  ngOnInit() {
    if (this.shouldInitDate) {
      this.dateModel = {
        isRange: false,
        singleDate: { date: { year: 1990, month: 12, day: 1 } }
      };
    } else {
      this.dateModel = { isRange: false, singleDate: { jsDate: new Date() } };
    }
  }
}
