import { Component, Input } from '@angular/core';

@Component({
  selector: 'select-honorific',
  styleUrls: ['./form-style.styl', './select-honorific.component.styl'],
  templateUrl: './select-honorific.component.html',
})
export class SelectHonorific {
  @Input() selections: string[];
  @Input() required: boolean;
}
