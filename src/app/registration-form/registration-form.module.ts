import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";

import { AngularMyDatePickerModule } from "angular-mydatepicker";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { RegistrationForm } from "./registration-form.component";

@NgModule({
  declarations: [RegistrationForm],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AngularMyDatePickerModule,
    FontAwesomeModule,
  ],
  providers: [],
  bootstrap: [RegistrationForm]
})
export class RegistrationFormModule {}
