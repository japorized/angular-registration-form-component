import { Component } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { IAngularMyDpOptions, IMyDateModel } from "angular-mydatepicker";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";

// Import fake data
import { emails } from "src/lib/emails";
import { newUsers } from "src/lib/new-registrations-db";

@Component({
  selector: "registration-form",
  templateUrl: "./registration-form.component.html",
  styleUrls: [
    "./registration-form.component.styl",
    "./form-components/form-style.styl",
    "./form-components/datetime.component.styl",
  ],
})
export class RegistrationForm {
  title = "angular-registration-form-component";
  registrationForm = new FormGroup({
    honorific: new FormControl(""),
    firstName: new FormControl(""),
    lastName: new FormControl(""),
    email: new FormControl(""),
    confirmEmail: new FormControl(""),
    dateOfBirth: new FormControl(""),
    phoneNumber: new FormControl("")
  });
  honorifics = ["Mr", "Mrs", "Prof"];
  validityMsgs = {
    firstName: "First name should only contain alphabets and whitespace",
    lastName:
      "Last name / Surname should only contain alphabets and whitespace",
    email: "",
    confirmEmail: "",
    phoneNumber: "Format: xxx-yyyyy-zzz"
  };
  // DatePicker variables
  shouldInitDate: boolean = true;
  dateModel: IMyDateModel = null;
  datePickerOptions: IAngularMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    dateRange: false,
    markCurrentDay: true,
    stylesData: {
      selector: "dp",
      styles: `
      .dp .myDpWeekDayTitle {
        background-color: deeppink;
      }
      `
    }
  };
  faCalendarAlt = faCalendarAlt;

  ngOnInit() {
    if (this.shouldInitDate) {
      this.dateModel = {
        isRange: false,
        singleDate: { date: { year: 1990, month: 12, day: 1 } }
      };
    } else {
      this.dateModel = { isRange: false, singleDate: { jsDate: new Date() } };
    }
  }

  // Form functions
  patternValidity(event: any) {
    const target: HTMLInputElement = event.target;
    const val: string = target.value;
    console.log(target.name);
    const tooltip = document.querySelector("#tooltip-" + target.name);
    if (/^[a-zA-Z\s]+$/.test(val) || val.length === 0) {
      tooltip.classList.remove("active");
    } else {
      tooltip.classList.add("active");
    }
  }

  checkEmailExistence(event: any) {
    const val = event.target.value;
    const tooltip = document.querySelector("#tooltip-email");
    if (emails.includes(val)) {
      this.validityMsgs.email = "Email address already exists";
      tooltip.classList.add("active");
    } else {
      tooltip.classList.remove("active");
    }
  }

  confirmMatchingEmail(event: any, originalValue: string) {
    const val = event.target.value;
    const tooltip = document.querySelector("#tooltip-confirm-email");
    if (originalValue !== val) {
      this.validityMsgs.confirmEmail = "Email address does not match";
      tooltip.classList.add("active");
    } else {
      tooltip.classList.remove("active");
    }
  }

  telFormatting(event: any) {
    // Allow user to completely remove their phone number
    if (event.key !== "Backspace") {
      const target = event.target;
      if (target.value.length === 3) {
        target.value = target.value.replace(/([0-9]{3})/, "$1-");
      } else if (target.value.length > 3 && target.value.length < 10) {
        target.value = target.value.replace(/([0-9]{3}-[0-9]{5})/, "$1-");
      }
    }
  }

  randomFillIn() {
    const newUser = newUsers[Math.floor(Math.random() * newUsers.length)];
    this.registrationForm.setValue(newUser);
    const userDOB = newUser.dateOfBirth.split('/').map((s: string) => Number(s));
    this.dateModel = {
      isRange: false,
      singleDate: { date: { year: userDOB[2], month: userDOB[1], day: userDOB[0] } }
    };
  }

  submission() {
    const submissionDOB = this.registrationForm.controls['dateOfBirth'].value['singleDate'].date;
    const user = {
      honorific: this.registrationForm.controls['honorific'].value,
      firstName: this.registrationForm.controls['firstName'].value,
      lastName: this.registrationForm.controls['lastName'].value,
      email: this.registrationForm.controls['email'].value,
      dateOfBirth: `${submissionDOB.day}/${submissionDOB.month}/${submissionDOB.year}`,
      phoneNumber: this.registrationForm.controls['phoneNumber'].value,
    };
    const rawUsers = window.sessionStorage.getItem('users');
    let users = [];
    if (rawUsers !== null) {
      users = JSON.parse(rawUsers);
      users.push(user);
    } else {
      users = [user];
    }
    window.sessionStorage.setItem('users', JSON.stringify(users));
    window.location.href += "/users";
  }
}
