import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RegistrationForm } from './registration-form/registration-form.component';
import { Users } from "./users/users.component";

const routes: Routes = [
  {
    path: "",
    component: RegistrationForm
  },
  {
    path: "users",
    component: Users
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
